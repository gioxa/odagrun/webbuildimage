[![](https://images.microbadger.com/badges/image/gioxa/odagrun-jekyll-build-image.svg)](https://microbadger.com/images/gioxa/odagrun-jekyll-build-image "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/gioxa/odagrun-jekyll-build-image.svg)](https://microbadger.com/images/gioxa/odagrun-jekyll-build-image "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/license/gioxa/odagrun-jekyll-build-image.svg)](https://microbadger.com/images/gioxa/odagrun-jekyll-build-image "Get your own license badge on microbadger.com")

# Build Image for www.odagrun.com

## purpose

Build image for static web development

CentOS7 docker image with:
- ruby 2.51
- bundle
- nodejs v10 +
- npm
- jekyll v3.8.4

## usage

For use with gitlab-ci

```yaml
image: gioxa/odagrun-jekyll-build-image:latest
```

# Security

All Native extensions build with CentOS Libs, up to date from the CentOS updates repository.


*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*
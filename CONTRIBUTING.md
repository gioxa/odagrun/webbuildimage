# All contributions welcome

- create an issue
- submit merge request
- Buy me a coffee [![buy me a coffee](https://www.buymeacoffee.com/assets/img/BMC-btn-logo.svg)](https://www.buymeacoffee.com/ryUMq1sSa) for some extra inspiration,
- or [![donate paypal](https://img.shields.io/badge/donate-PayPal-blue.svg)](https://www.paypal.me/gioxa) to keep the cogs turning.

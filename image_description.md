# ${ODAGRUN_IMAGE_REFNAME}


[![](https://images.microbadger.com/badges/version/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/image/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}) [![](https://images.microbadger.com/badges/license/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME}.svg)](https://microbadger.com/images/${DOCKER_NAMESPACE}/${ODAGRUN_IMAGE_REFNAME} "Get your own license badge on microbadger.com") [![](https://img.shields.io/badge/Gitlab-source-blue.svg)](${CI_PROJECT_URL})

## Description

CentOS7 docker image with:
- ruby 2.53
- bundle
- nodejs V10.+
- npm
- jekyll 3.8.4

To build website [www.odagrun.com](https://www.odagrun.com)

## installed ruby packages

```bash
# GEMS:

$GEM_LIST
```

## installed node packages:

- npm
- purgecss


## Image Config

```yaml
$DOCKER_CONFIG_YML
```

## usage

For use with gitlab-ci

```yaml
image: gioxa/odagrun-jekyll-build-image:latest
```

# Security

All Native extensions build with CentOS Libs, up to date from the updates repository.

---

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*